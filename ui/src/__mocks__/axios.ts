import Axios from "axios";

const mockAxios: jest.Mocked<typeof Axios> = jest.genMockFromModule("axios");

mockAxios.get.mockImplementation(() => Promise.resolve({ data: "Hello!" }));

export default mockAxios;
