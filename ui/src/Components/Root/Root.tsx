import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import { SplashPageComponent, NotFoundPageComponent } from "../../Pages";

export class Root extends Component {
  render() {
    return (
      <Switch>
        <Route path="/" exact={true} component={SplashPageComponent} />
        <Route component={NotFoundPageComponent} />
      </Switch>
    );
  }
}
