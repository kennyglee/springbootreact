import React, { Component } from "react";
import { Card, CardContent, CardHeader } from "@material-ui/core";

export class PokemonCard extends Component {
  render() {
    return (
      <Card id="pokemon_card_0">
        <CardHeader title="Pokemon Card" />
        <CardContent id="pokemon_name_0">Pokemon Name</CardContent>
        <CardContent id="pokemon_type_0">Pokemon Type</CardContent>
        <CardContent id="pokemon_level_0">Pokemon Level</CardContent>
        <CardContent id="pokemon_height_0">Pokemon Height</CardContent>
        <CardContent id="pokemon_gender_0">Pokemon Gender</CardContent>
        <CardContent id="pokemon_hp_0">Pokemon HP</CardContent>
      </Card>
    );
  }
}
