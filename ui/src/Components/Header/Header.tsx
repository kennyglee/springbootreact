import React from "react";
import logo from "../../Assets/logo.svg";
import "./Header.css";

export const Header: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
  );
};
