import React from "react";
import { ReactWrapper, mount } from "enzyme";
import { SplashPageComponent } from "../../../Pages";
import mockAxios from "axios";

describe("Splash Page Tests", () => {
  let fullMount: ReactWrapper<{}, {}, SplashPageComponent>;

  beforeAll(() => {
    fullMount = mount(<SplashPageComponent />);
  });

  afterAll(() => {
    fullMount.unmount();
  });

  it("Splash Page Renders DOM", () => {
    const splashPageNode = fullMount.find("#splash_page_id");
    expect(splashPageNode).toExist();
  });

  it("Splash page calls hello endpoint on mount", () => {
    expect(mockAxios.get).toHaveBeenCalled();
  });

  it("Splash page renders text received from the hello enpoint", () => {
    const textHolder = fullMount.find("#hello_text_holder");
    expect(textHolder).toExist();
    expect(textHolder.hostNodes()).toHaveText("Hello!");
  });
});
