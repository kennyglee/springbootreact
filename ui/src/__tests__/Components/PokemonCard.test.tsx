import React from "react";
import { PokemonCard } from "../../Components";
import { ShallowWrapper, shallow } from "enzyme";

describe("Pokemon Card Tests", () => {
  let shallowMount: ShallowWrapper;

  beforeAll(() => {
    shallowMount = shallow(<PokemonCard />);
  });
  it("Renders When Mounted", () => {
    expect(shallowMount).toExist();
  });

  it("Pokemon Card Component is rendered", () => {
    let pokemonCardComponent = shallowMount.find("#pokemon_card_0");
    expect(pokemonCardComponent).toExist();
  });

  it("Renders all of the entity properties into respective cards", () => {
    let id = 0;
    let pokemonCardComponent = shallowMount.find("#pokemon_card_" + id);
    expect(pokemonCardComponent).toExist();

    let pokemonNameField = pokemonCardComponent.find("#pokemon_name_" + id);
    let pokemonTypeField = pokemonCardComponent.find("#pokemon_type_" + id);
    let pokemonGenderField = pokemonCardComponent.find("#pokemon_gender_" + id);
    let pokemonHeightField = pokemonCardComponent.find("#pokemon_height_" + id);
    let pokemonLevelField = pokemonCardComponent.find("#pokemon_level_" + id);
    let pokemonHPField = pokemonCardComponent.find("#pokemon_hp_" + id);

    let fields = [];
    fields.push(
      pokemonNameField,
      pokemonTypeField,
      pokemonGenderField,
      pokemonHPField,
      pokemonHeightField,
      pokemonLevelField,
    );
    fields.forEach((field) => expect(field).toExist());
  });
});
