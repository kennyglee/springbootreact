import React, { Component } from "react";
import { Grid, Typography } from "@material-ui/core";
import Axios from "axios";
import { PokemonCard } from "../../Components";

export class SplashPageComponent extends Component<any, any> {
  constructor(props: any) {
    super(props);

    this.state = {
      message: "",
    };
  }
  async getGreeting() {
    try {
      let response = await Axios.get("/api/hello");
      let { data } = response;
      this.setState({ message: data });
    } catch (err) {
      console.error("getGreeting Error", err);
    }
  }

  componentDidMount() {
    this.getGreeting();
  }

  render() {
    // const handleForce = (data) => {
    //   console.log(data);
    // };

    return (
      <Grid
        id="splash_page_id"
        container
        direction="column"
        alignItems="center"
        alignContent="center"
      >
        <Grid item xs={12}>
          <Typography variant="h4">Splash Page Component</Typography>
        </Grid>
        <Grid item xs>
          <Typography id="hello_text_holder" variant="h5">
            {this.state.message}
          </Typography>
        </Grid>
        <Grid item xs>
          <PokemonCard />
        </Grid>

        <Grid item xs>
          {/* <CSVReader
            cssClass="csv-reader-input"
            label="Select CSV with secret Death Star statistics"
            onFileLoaded={this.handleForce}
            onError={this.handleDarkSideForce}
            inputId="ObiWan"
            inputStyle={{ color: "red" }}
          /> */}
          {/* ) */}
        </Grid>
      </Grid>
    );
  }
}
