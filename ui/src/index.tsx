import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter as Router } from "react-router-dom";
import { Root } from "./Components/Root";
import { Header } from "./Components";

const renderApplication = (Root: any) => {
  ReactDOM.render(
    <div>
      <Header />
      <Router>
        <Root />
      </Router>
    </div>,
    document.getElementById("root"),
  );
};

renderApplication(Root);

// ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
