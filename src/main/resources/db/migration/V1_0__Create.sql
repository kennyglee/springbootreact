CREATE TABLE `pokemon_db`.`pokemon` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  `height` INT NOT NULL,
  `gender` VARCHAR(45) NOT NULL,
  `level` INT NOT NULL,
  `HP` INT NOT NULL,
  PRIMARY KEY (`id`));
