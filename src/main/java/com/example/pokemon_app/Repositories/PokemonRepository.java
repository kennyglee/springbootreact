package com.example.pokemon_app.Repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.pokemon_app.Entities.PokemonEntity;


/**
 * PokemonRepository
 */
public interface PokemonRepository extends JpaRepository<PokemonEntity,Integer>{

  
}