package com.example.pokemon_app.Controllers;

import java.util.List;

import com.example.pokemon_app.Entities.PokemonEntity;
import com.example.pokemon_app.Repositories.PokemonRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * PokemonController
 */
@RestController
@RequestMapping("/api")
@CrossOrigin(origins={"localhost:3000"})
public class PokemonController {

  @Autowired
  PokemonRepository pokemonRepository;

  @GetMapping("/hello")
  public String helloGreeting(){
    return "Hello!";
  }
  
  @PostMapping("/addPokemon")
  public PokemonEntity addPokemon(@RequestBody PokemonEntity pokemonToAdd){
    return pokemonRepository.save(pokemonToAdd);
  }

  @GetMapping("/getAllPokemon")
  public List<PokemonEntity> getAllPokemon(){
    return pokemonRepository.findAll();
  }
  
}