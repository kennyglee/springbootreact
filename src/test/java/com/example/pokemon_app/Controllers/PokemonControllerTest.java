package com.example.pokemon_app.Controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.List;

import com.example.pokemon_app.Entities.PokemonEntity;
import com.example.pokemon_app.Repositories.PokemonRepository;

import org.junit.Test;
// import org.mockito.junit.MockitoJUnitRunner;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * PokemonControllerTest
 */
@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class PokemonControllerTest {

  @Mock
  PokemonRepository pokemonRepository;

  @InjectMocks
  PokemonController pokemonController = new PokemonController();
  
  private PokemonEntity testPokemon = PokemonEntity.builder().id(1).name("Squirtle").hp(777).height(100).gender("bro").level(1).type("water").build();
  private List<PokemonEntity> testPokemonList = Collections.singletonList(testPokemon);
  @Test
  public void helloGreeting(){
    String returnedString = pokemonController.helloGreeting();
    assertEquals("Hello!", returnedString);
  }

  @Test
  public void addPokemon(){
    when(pokemonRepository.save(testPokemon)).thenReturn(testPokemon);
    PokemonEntity returnedPokemon = pokemonController.addPokemon(testPokemon);
    assertEquals(testPokemon, returnedPokemon);
  }

  @Test
  public void getAllPokemon(){
    when(pokemonRepository.findAll()).thenReturn(testPokemonList);
    List<PokemonEntity> returnedPokemon = pokemonController.getAllPokemon();
    assertIterableEquals(testPokemonList, returnedPokemon);
  }
  
}